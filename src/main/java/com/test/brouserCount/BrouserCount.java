package com.test.brouserCount;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.io.compress.SnappyCodec;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;

public class BrouserCount {

    /** Mapper class */
    public static class TokenizerMapper
            extends Mapper<Object, Text, Text, IntWritable>{

        private final static IntWritable one = new IntWritable(1);
        private Text word = new Text();

        public void map(Object key, Text value, Context context
        ) throws IOException, InterruptedException {

            /* Split text to strings */
            String[] split = value.toString().split("\n");
            for (String line : split) {
                String brouser = getBrouserFromString(line);

                /* If string is incorrect pass it */
                if (brouser == null) {
                    word.set("ERROR");
                } else {
                    word.set(brouser);
                }

                context.write(word, one);
            }
        }

        /** Function that get brouser from string. If the input string is incorrect return value is null */
        public String getBrouserFromString(String string) {
            if (string.length() == 0) {
                return null;
            }
            String[] sl = string.split(" ");
            if (sl.length < 2) {
                return null;
            }
            String brouser = sl[sl.length - 1];
            if (brouser.toLowerCase().contains(("version"))) {
                return null;
            }

            if ((brouser.substring(brouser.length()-1).equalsIgnoreCase("\"")))
            {
                brouser = brouser.replace(brouser.substring(brouser.length()-1), "");
            }

            return brouser;
        }
    }


    /** Reducer class */
    public static class IntSumReducer
            extends Reducer<Text,IntWritable,Text,IntWritable> {
        private IntWritable result = new IntWritable();

        public void reduce(Text key, Iterable<IntWritable> values,
                           Context context
        ) throws IOException, InterruptedException {
            int sum = 0;
            for (IntWritable val : values) {
                sum += val.get();
            }
            result.set(sum);
            context.write(key, result);
        }
    }

    /** Main function do configure work */
    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "brouser count");
        job.setJarByClass(BrouserCount.class);
        job.setMapperClass(TokenizerMapper.class);
        job.setCombinerClass(IntSumReducer.class);
        job.setReducerClass(IntSumReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        SequenceFileOutputFormat.setOutputPath( job, new Path(args[1]) );
        SequenceFileOutputFormat.setCompressOutput( job, true );
        SequenceFileOutputFormat.setOutputCompressorClass( job, SnappyCodec.class );

        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
