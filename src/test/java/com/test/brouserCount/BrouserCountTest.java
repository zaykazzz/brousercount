package com.test.brouserCount;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.Text;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import java.util.ArrayList;

/**
 * Unit test for simple App.
 */
public class BrouserCountTest
    extends TestCase
{
    MapDriver<Object, Text, Text, IntWritable> mapDriver;
    com.test.brouserCount.BrouserCount.TokenizerMapper mapper;
    ReduceDriver<Text, IntWritable, Text, IntWritable> reduceDriver;
    com.test.brouserCount.BrouserCount.IntSumReducer reducer;

    public void setUp()
    {
        /* setup mapper */
        mapper = new com.test.brouserCount.BrouserCount.TokenizerMapper();
        mapDriver = MapDriver.newMapDriver(mapper);

        /* setup reducer */
        reducer = new com.test.brouserCount.BrouserCount.IntSumReducer();
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
    }

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public BrouserCountTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( BrouserCountTest.class );
    }

    public void testMapPart() throws Exception {
        String inputString = "188.192.63.123 - - [24/Oct/2017:23:52:25 +0300] \"PUT /posts/posts/explore HTTP/1.0\" 200 4969 \"http://king-rice.org/faq.asp\" \"Mozilla/5.0 (X11; Linux i686; rv:1.9.5.20) Gecko/2013-11-17 18:08:29 Firefox/14.0\"";
        mapDriver.withInput(new LongWritable(),new Text(inputString));
        mapDriver.withOutput(new Text("Firefox/14.0"), new IntWritable(1));

        mapDriver.runTest();
    }

    /** Method to test empty string */
    public void testGetBrouserFromEmptyString() throws Exception
    {
        String result = mapper.getBrouserFromString("");
        assertEquals(null,result);
    }

    /** Method to test string that contains one word */
    public void testGetBrouserFromStringWithOneWord() throws Exception
    {
        String result = mapper.getBrouserFromString("qwerty");
        assertEquals(null,result);
    }

    /** Method to test wrong string */
    public void testGetBrouserFromStringWithVersion() throws Exception
    {
        String result = mapper.getBrouserFromString("sdfg version");
        assertEquals(null,result);
    }

    /** Method to test normal string */
    public void testGetBrouserFromNormalString() throws Exception
    {
        String result = mapper.getBrouserFromString("0) Gecko/2013-11-17 18:08:29 Firefox/14.0");
        assertEquals("Firefox/14.0",result);
    }

    /** Method to test normal string with final bracket */
    public void testGetBrouserFromNormalStringWithFinalBracket() throws Exception
    {
        String result = mapper.getBrouserFromString("0) Gecko/2013-11-17 18:08:29 Firefox/14.0\"");
        assertEquals("Firefox/14.0",result);
    }

    /** Test Reducer */
    public void testReducePart() throws Exception {
        ArrayList<IntWritable> array = new ArrayList<IntWritable>();
        array.add(new IntWritable(1));

        reduceDriver.withInput(new Text("Firefox/14.0"), array);
        array.add(new IntWritable(1));
        reduceDriver.withInput(new Text("Safari"), array);

        reduceDriver.withOutput(new Text("Firefox/14.0"), new IntWritable(1));
        reduceDriver.withOutput(new Text("Safari"), new IntWritable(2));

        reduceDriver.runTest();
    }

}
