hadoop fs -rm -r /WordCountDirs/outputDir
echo "Removing output directory..."
hadoop jar target/brousercount-1.0-SNAPSHOT.jar com/test/brouserCount/BrouserCount /WordCountDirs/inputDir/ /WordCountDirs/outputDir/
echo "Hadoop work complete"
echo "Removing output file in local directory"
rm -f ./outputFile
hadoop fs -text /WordCountDirs/outputDir/part* > ./outputFile
echo "Results saved in ./outputFile"
